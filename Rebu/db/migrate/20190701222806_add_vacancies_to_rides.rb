class AddVacanciesToRides < ActiveRecord::Migration[5.2]
  def change
    add_column :rides, :vacancies, :integer
  end
end
