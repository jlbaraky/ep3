class CreateRides < ActiveRecord::Migration[5.2]
  def change
    create_table :rides do |t|
      t.string :origin
      t.string :destination
      t.string :weekday
      t.string :time
      t.float :price
      t.text :note
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
